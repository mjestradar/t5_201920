package model.logic;


import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import com.opencsv.CSVReader;


import model.data_structures.*;
import model.util.Sort;
import mundo.VOMovingViolations;






/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	/**
	 * Atributos del modelo del mundo
	 */


	private ArregloDinamico<TravelTime> datos;
	private ArrayList<TravelTime> viajes;
	TravelTime[] muestra;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new ArregloDinamico<>(200000);
	}






	public int cargar() {
		int rta =0;
        
		 
		CSVReader reader = null;
		try 
		{
         for (int i =1; i<5;i++)
         {	reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+i+"-All-HourlyAggregate.csv"));
			String[] nextLine=reader.readNext();
			while ((nextLine = reader.readNext()) != null) 
			{ArrayList<TravelTime> nik= new ArrayList<>();
				TravelTime elem = new TravelTime(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), 
						Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]),
						Double.parseDouble(nextLine[4]));
				 viajes.add(elem);
                nik.add(elem);
                System.out.println("El primer viaje del trimestre "+i+" tiene los atributos : "+nik.get(0).getSourceId()+" "+ nik.get(0).getDstId()+" "+ nik.get(0).getDow()+" "+nik.get(0).getMeanTravelTime());                
                System.out.println("El ultimo viaje del trimestre "+i+" tiene los atributos : "+nik.get(nik.size()-1).getSourceId()+" "+ nik.get(nik.size()-1).getDstId()+" "+ nik.get(nik.size()-1).getDow()+" "+nik.get(nik.size()-1).getMeanTravelTime());
				rta++;



			}
         }


		} 

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			if (reader != null) 
			{
				try 
				{
					reader.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		muestra=new TravelTime[datos.darTamano()];
		for(int i  = 0; i < datos.darTamano(); i++)
		{
			muestra[i] = datos.darElemento(i);
		}
		
		return rta;
	}
	
}