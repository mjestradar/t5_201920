package model.logic;

public class TravelTime implements Comparable<TravelTime>{
	
	
	private int sourceId;
	
	private int dstId;
	
	private int dow;
	
	private double meanTravelTime;
	
	private double standardDeviationTravelTime;

	
	
	public TravelTime(int a, int b, int c, double d, double e) {
		sourceId=a;
		dstId=b;
		dow=c;
		meanTravelTime=d;
		standardDeviationTravelTime=e;
	
	}


	public int getSourceId() {
		return sourceId;
	}


	public int getDstId() {
		return dstId;
	}


	public int getDow() {
		return dow;
	}


	public double getMeanTravelTime() {
		return meanTravelTime;
	}


	public double getStandardDeviationTravelTime() {
		return standardDeviationTravelTime;
	}


	@Override
	public int compareTo(TravelTime o) {
		int retorno = (int) (meanTravelTime - o.getMeanTravelTime());
        
		
		
			return retorno;
		

	}


	
}
