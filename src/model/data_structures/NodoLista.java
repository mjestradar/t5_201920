package model.data_structures;

public class NodoLista<T> 
{

	private T elemento;

	private NodoLista<T> siguiente;

	public NodoLista(T dato)
	{
		elemento = dato;
		siguiente = null;
	}

	public void cambiarSiguiente(NodoLista<T> dato)
	{
		siguiente = dato;
	}
	public NodoLista<T> darSiguiente()
	{
		return siguiente;
	}
	public T darElemento()
	{
		return elemento;
	}
}
